
from distutils.command.config import config
import logging
from telegram.constants   import ParseMode
import jalali

from config import BotConfig

from telegram import __version__ as TG_VER

try:
    from telegram import __version_info__
except ImportError:
    __version_info__ = (0, 0, 0, 0, 0)  # type: ignore[assignment]

if __version_info__ < (20, 0, 0, "alpha", 1):
    raise RuntimeError(
        f"This example is not compatible with your current PTB version {TG_VER}. To view the "
        f"{TG_VER} version of this example, "
        f"visit https://docs.python-telegram-bot.org/en/v{TG_VER}/examples.html"
    )
from telegram import Update
from telegram.ext import Application, CommandHandler, ContextTypes

# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)



from asgiref import sync

# import time
import asyncio
import requests
from datetime import datetime
# @timed
async def async_requests_post_all(datas):
    """
    asynchronous wrapper around synchronous requests
    """
    session = requests.Session()
    # wrap requests.get into an async function
    def get(data):
        url = 'https://safarmarket.com/api/flight/v3/search' 
        myJson = {
            "platform":"WEB_DESKTOP",
            "uid":"",
            "limit":250,
            "compress":False,
            "productType":"IFLI",
            "searchValidity":2,
            "cid":1,
            "checksum":1,
            "IPInfo":{"ip":"11.12.13.14","isp":"a","city":"teh","country":"ir"},
            "searchFilter":
                {
                    "sourceAirportCode":"THR",
                    "targetAirportCode":"IST",
                    "sourceIsCity":True,
                    "targetIsCity":True,
                    "leaveDate": data[0],
                    "returnDate":"",
                    "adultCount":1,
                    "childCount":0,
                    "infantCount":0,
                    "economy":True,
                    "business":True
                }
        }
        return session.post(url, json=myJson).json()
    async_get = sync.sync_to_async(get)

    async def get_all(datas):
        return await asyncio.gather(*[
            async_get(data) for data in datas
        ])
    # call get_all as a sync function to be used in a sync context
    return await get_all(datas)
from datetime import date, timedelta

def daterange(start_date, end_date):
    for n in range(int((end_date - start_date).days)):
        yield start_date + timedelta(n)


async def get_cheapest_ticket():
    print("in the method 1")
    urls = []
    user_date = date(2022, 11, 16)
    # start_date = user_date if user_date else datetime.now().date()
    start_date = max(user_date, datetime.now().date())
    print(f'start_date {start_date}')

    end_date = date(2022, 11, 23)

    for single_date in daterange(start_date, end_date):
        leave_date = single_date.strftime("%Y-%m-%d")
        return_date = (single_date + timedelta(days=4)).strftime("%Y-%m-%d")
        urls.append([leave_date, return_date])

    results = await async_requests_post_all(urls)

    print(len(results))
    text = ''
    for result in results:
        try:

            flight = result['result']['flights'][0]
            providers = flight['providers']

            leave_date = flight['leave']['departureTime']
            return_date = flight['return']['departureTime']
            price = int(flight['minPrice'])
            # print(price)
            text += f'''
📅 روز حرکت: 
{jalali.Gregorian(leave_date.split()[0]).persian_string() }
⏰ ساعت حرکت: 
{leave_date.split()[1]}
💲 قیمت: 
'''
            text += "{:,}".format(price//10) + " تومان"

            text += "\nلینک‌های خرید:\n"
            for provider in providers:
                text += f'''🔗 [{provider['title']}]({provider['url']})\n'''

        except Exception as e: 
            text += 'پروازی یافت نشد\n'
            text += str(result)
        text += '🟥🟥🟥🟥🟥🟥'
    print(text)

        
# 📅 روز بازگشت: 
# {jalali.Gregorian(return_date.split()[0]).persian_string() }
# ⏰ ساعت بازگشت: 
# {return_date.split()[1]}
    return text
# Define a few command handlers. These usually take the two arguments update and
# context.
# Best practice would be to replace context with an underscore,
# since context is an unused local variable.
# This being an example and not having context present confusing beginners,
# we decided to have it present as context.
async def start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Sends explanation on how to use the bot."""
    await update.message.reply_text("Hi! Use /set <seconds> to set a timer")


async def alarm(context: ContextTypes.DEFAULT_TYPE) -> None:
    """Send the alarm message."""
    job = context.job

    texts = await get_cheapest_ticket()

    await context.bot.send_message(job.chat_id, text=texts, parse_mode=ParseMode.MARKDOWN)


def remove_job_if_exists(name: str, context: ContextTypes.DEFAULT_TYPE) -> bool:
    """Remove job with given name. Returns whether job was removed."""
    current_jobs = context.job_queue.get_jobs_by_name(name)
    if not current_jobs:
        return False
    for job in current_jobs:
        job.schedule_removal()
    return True


async def set_timer(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Add a job to the queue."""
    chat_id = update.effective_message.chat_id
    try:
        # args[0] should contain the time for the timer in seconds
        due = float(context.args[0])
        if due < 0:
            await update.effective_message.reply_text("Sorry we can not go back to future!")
            return

        job_removed = remove_job_if_exists(str(chat_id), context)
        context.job_queue.run_repeating(alarm, due, chat_id=chat_id, name=str(chat_id), data=due)

        text = "Timer successfully set!"
        if job_removed:
            text += " Old one was removed."
        await update.effective_message.reply_text(text)

    except (IndexError, ValueError):
        await update.effective_message.reply_text("Usage: /set <seconds>")


async def unset(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Remove the job if the user changed their mind."""
    chat_id = update.message.chat_id
    job_removed = remove_job_if_exists(str(chat_id), context)
    text = "Timer successfully cancelled!" if job_removed else "You have no active timer."
    await update.message.reply_text(text)


def main() -> None:
    """Run bot."""
    # Create the Application and pass it your bot's token.
    application = Application.builder().token(BotConfig.token).build()

    # on different commands - answer in Telegram
    application.add_handler(CommandHandler(["start", "help"], start))
    application.add_handler(CommandHandler("set", set_timer))
    application.add_handler(CommandHandler("unset", unset))

    # Run the bot until the user presses Ctrl-C
    application.run_polling()


if __name__ == "__main__":
    main()